/**
 * @author: semenov
 */
public class ActionGrenade extends Action {
    private final Point point;

    public ActionGrenade(Point en, Action from) {
        super(from);
        point = en;
    }

    @Override
    public boolean run(TrooperStrategy ts) {
        return ts.grenade(point);
    }

    @Override
    public int cost(Action prev) {
        return overMind.game.getGrenadeThrowCost();
    }
}
