import model.Game;
import model.Move;
import model.Trooper;
import model.World;

public final class MyStrategy implements Strategy {
    private static final OverMind overMind = new OverMind();

    @Override
    public void move(Trooper self, World world, Game game, Move move) {
        try {
            overMind.init(self, world, game, move);
        } catch (Exception e) {
            overMind.debugLog(e);
        }
    }
}
