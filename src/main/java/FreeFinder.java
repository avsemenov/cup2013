import model.Trooper;
import model.TrooperStance;

public class FreeFinder {
    private OverMind overMind;

    public FreeFinder(OverMind overMind) {
        this.overMind = overMind;
    }

    public Point getFreePoint(Point t) {
        return get(t.x,t.y,false);
    }

    public Point getFreePoint(int tx, int ty) {
        return get(tx,ty,false);
    }

    public Point getFreeAndSafe(int tx, int ty) {
        return get(tx,ty,true);
    }

    public boolean testSafe(int x, int y) {
        for(Enemy tr: overMind.getEnemies()) {
            if (overMind.world.isVisible(tr.trooper.getVisionRange(),tr.trooper.getX(),tr.trooper.getY(),tr.trooper.getStance(),x,y, TrooperStance.STANDING)) return false;
        }
        return true;
    }

    public Point get(int tx, int ty, boolean safe) {
        if (overMind.isFreePoint(tx, ty)) return new Point(tx, ty, TrooperStance.STANDING);
        int i = 0;
        int all = 4;
        while (all > 0) {
            i++;
            all = 0;
            int minX = tx - i;
            if (minX < 0) minX = 0;
            int minY = ty - i;
            if (minY < 0) minY = 0;
            int maxX = tx + i;
            if (maxX >= overMind.world.getHeight()) maxX = overMind.world.getHeight() - 1;
            int maxY = ty + i;
            if (maxY >= overMind.world.getWidth()) maxY = overMind.world.getWidth() - 1;

            if (ty - i >= 0) {
                for (int x = minX; x < maxX; x++) {
                    if (overMind.isFreePoint(x, minY)) return new Point(x, minY, TrooperStance.STANDING);
                }
                all++;
            }
            if (ty + i < overMind.world.getWidth()) {
                for (int x = minX; x < maxX; x++) {
                    if (overMind.isFreePoint(x, maxY)) return new Point(x, maxY, TrooperStance.STANDING);
                }
                all++;
            }
            if (tx - i >= 0) {
                for (int y = minY + 1; y < maxY - 1; y++) {
                    if (overMind.isFreePoint(minX, y)) return new Point(minX, y, TrooperStance.STANDING);
                }
                all++;
            }
            if (maxX < overMind.world.getHeight()) {
                for (int y = minY + 1; y < maxY - 1; y++) {
                    if (overMind.isFreePoint(maxX, y)) return new Point(maxX, y, TrooperStance.STANDING);
                }
                all++;
            }
        }
        return null;
    }

}
