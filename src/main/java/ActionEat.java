/**
 * @author: semenov
 */
public class ActionEat extends Action {
    public ActionEat(Point p) {
        super(p);
    }

    @Override
    public boolean run(TrooperStrategy ts) {
        return ts.eatFieldRation();
    }

    @Override
    public int cost(Action prev) {
        return - overMind.game.getFieldRationBonusActionPoints() + overMind.game.getFieldRationEatCost();
    }
}
