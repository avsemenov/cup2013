import model.Trooper;
import model.TrooperStance;
import model.Unit;

import java.util.ArrayList;
import java.util.List;

import static java.lang.StrictMath.hypot;

/**
 * @author: semenov
 */
public class Point {
    public int x;
    public int y;
    public TrooperStance z = TrooperStance.PRONE;
    static OverMind overMind;

    public static void setOverMind(OverMind overMind) {
        Point.overMind = overMind;
    }

    public double getDistanceTo(Point p) {
        return hypot(p.x - this.x, p.y - this.y);
    }

    //свободные точки
    public List<Point> getFreeAround() {
        return getAround(false);
    }

    //свободные точки на плоскости
    public List<Point> getFreeAround2D() {
        return getAround2D(false);
    }

    //точки по карте на плоскости
    public List<Point> getMapAround2D() {
        return getAround2D(true);
    }

    //точки по карте
    public List<Point> getMapAround() {
        return getAround(true);
    }

    private List<Point> getAround(boolean noTroopers) {
        List<Point> list = new ArrayList(6);
        if (nextLoverStance() != null) list.add(new Point(x, y, nextLoverStance()));
        if (nextUpperStance() != null) list.add(new Point(x, y, nextUpperStance()));
        if (overMind.isFreePoint(x + 1, y, noTroopers)) list.add(new Point(x + 1, y, z));
        if (overMind.isFreePoint(x - 1, y, noTroopers)) list.add(new Point(x - 1, y, z));
        if (overMind.isFreePoint(x, y - 1, noTroopers)) list.add(new Point(x, y - 1, z));
        if (overMind.isFreePoint(x, y + 1, noTroopers)) list.add(new Point(x, y + 1, z));
        return list;
    }

    private List<Point> getAround2D(boolean noTroopers) {
        List<Point> list = new ArrayList(4);
        if (overMind.isFreePoint(x + 1, y, noTroopers)) list.add(new Point(x + 1, y, null));
        if (overMind.isFreePoint(x - 1, y, noTroopers)) list.add(new Point(x - 1, y, null));
        if (overMind.isFreePoint(x, y - 1, noTroopers)) list.add(new Point(x, y - 1, null));
        if (overMind.isFreePoint(x, y + 1, noTroopers)) list.add(new Point(x, y + 1, null));
        return list;
    }

    public TrooperStance nextLoverStance() {
        if (this.z == TrooperStance.STANDING) return TrooperStance.KNEELING;
        if (this.z == TrooperStance.KNEELING) return TrooperStance.PRONE;
        return null;
    }

    public TrooperStance nextUpperStance() {
        if (this.z == TrooperStance.PRONE) return TrooperStance.KNEELING;
        if (this.z == TrooperStance.KNEELING) return TrooperStance.STANDING;
        return null;
    }

    public Point(int x, int y, TrooperStance z) {
        setPoint(x, y, z);
    }

    public Point(Unit u) {
        setPoint(u);
    }

    public void setPoint(int x, int y, TrooperStance z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setPoint(Unit u) {
        this.x = u.getX();
        this.y = u.getY();
        if (u instanceof Trooper) z = ((Trooper) u).getStance();
    }

    @Override
    public int hashCode() {
        if (z != null) return x << 8 + y << 16 + z.ordinal();
        return x << 8 + y << 16;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Point)) return false;
        Point p = (Point) obj;
        return p.x == x && p.y == y && (p.z == z || z == null || p.z == null);
    }

    public boolean equals2D(Point p) {
        return p.x == x && p.y == y;
    }

    @Override
    public String toString() {
        return x + "," + y + "," + z;
    }

    public int distance(Point point) {
        if (point.x == x && point.y == y) {
            if (point.z == z) return 0;
            return overMind.game.getStanceChangeCost();
        }
        return overMind.getMoveCost(z);
    }


}
