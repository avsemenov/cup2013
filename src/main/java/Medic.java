import model.*;

/**
 * @author: semenov
 */
public class Medic extends TrooperStrategy {
    public Medic(OverMind overMind) {
        super(overMind);
    }

    protected void freeMode() {
        if (findAndHeall()) return;
        if (moveToFriend()) return;
        if (findBonus()) return;
        if (randomMove()) return;
    }

    protected void attackMode() {
        if (isAlone()) {
            if (findAndGrenade()) return;
            if (findAndKill()) return;
            if (findSafePoint()) return;
            if (findAndHeall()) return;
        } else {
            if (findSafePoint()) return;
            if (findAndHeall()) return;
            if (findAndGrenade()) return;
//            if (findAndKill()) return;
            if (moveToFriend()) return;
        }
    }

    private boolean findSafePoint() {
        overMind.debugLog(" findSafePoint ");
        if (trooper.getMaximalHitpoints() - trooper.getHitpoints() >
                game.getFieldMedicHealSelfBonusHitpoints() * (getMoveCost() / game.getFieldMedicHealCost())
                && !overMind.isSafe(getPoint())) {
            Path path = overMind.pf.getWayToSafePoint(new Point(trooper), trooper.getActionPoints());
            return moveToPath(path);
        }
        return false;
    }


    private boolean findAndHeall() {
        overMind.debugLog(" findAndHeall ");
        if (overMind.isWounded()) {
            Trooper wounded = null;
            Path minDist = null;
            for (Trooper w : overMind.wounded) {
                if (w.getId() == trooper.getId()) return heal(w);
                Path path = overMind.pf.getWayBefore(getPoint(), new Point(w), false, 200);
                if (path != null) {
                    if (minDist == null || path.cost() < minDist.cost()) {
                        minDist = path;
                        wounded = w;
                    }
                }
            }
            if (wounded != null) {
                if (minDist.cost() == 0) {
                    if (heal(wounded)) return true;
                } else {
                    return moveToPath(minDist.trunkToCost(trooper.getActionPoints()));
                }
            }
        }
        return false;
    }

    @Override
    boolean needHeal() {
        return false;
    }

    private boolean heal(Trooper w) {
        if (game.getFieldMedicHealCost() > trooper.getActionPoints()) return false;
        if (trooper.isHoldingMedikit()
                && game.getMedikitBonusHitpoints() <= w.getMaximalHitpoints() - w.getHitpoints()
                && game.getMedikitUseCost() <= trooper.getActionPoints()) {
            overMind.debugLog("Heal MK (%d,%d) %s", w.getX(), w.getY(), w.getType().toString());
            move.setAction(ActionType.USE_MEDIKIT);
        } else {
            overMind.debugLog("Heal (%d,%d) %s", w.getX(), w.getY(), w.getType().toString());
            move.setAction(ActionType.HEAL);
        }
        if (w.getId() == trooper.getId()) {
            move.setDirection(Direction.CURRENT_POINT);
        } else if (w.getX() == trooper.getX()) {
            move.setDirection(w.getY() > trooper.getY() ? Direction.SOUTH : Direction.NORTH);
        } else {
            move.setDirection(w.getX() > trooper.getX() ? Direction.EAST : Direction.WEST);
        }
        return true;
    }

    public TrooperStance getMaxStance() {
        return TrooperStance.STANDING;
//        return TrooperStance.KNEELING;
    }
}
