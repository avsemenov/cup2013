package model;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author: semenov
 */
public class GUI {
    PrintWriter out = null;
    private boolean init = true;
    Socket echoSocket;

    @Override
    protected void finalize() throws Throwable {
        echoSocket.close();
        super.finalize();
    }

    public GUI() {
        try {
            echoSocket = new Socket("localhost", 6789);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
        } catch (IOException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void draw(World world) {
        if (out==null)return;
        if (init) {
            init = false;
            for (int x = 0; x < world.getWidth(); x++) {
                for (int y = 0; y < world.getHeight(); y++) {
                    CellType wall = world.getCells()[x][y];
                    if (wall==CellType.FREE) continue;
                    if (wall==CellType.HIGH_COVER) {
                        drawWall(x*world.getHeight()+y,x,y,200,200,200);
                        continue;
                    }
                    if (wall==CellType.MEDIUM_COVER) {
                        drawWall(x*world.getHeight()+y,x,y,150,150,150);
                        continue;
                    }
                    if (wall==CellType.LOW_COVER) {
                        drawWall(x*world.getHeight()+y,x,y,100,100,100);
                    }
                }
            }
        } else {
            out.println("clear");
        }
        for (Trooper tr : world.getTroopers()) {
            if (tr.isTeammate()) {
                draw(tr.getId(), tr.getX(), tr.getY(), 0, 0, 255);
            } else {
                draw(tr.getId(), tr.getX(), tr.getY(), 255, 0, 0);
            }
        }
        out.println("refresh");
    }

    private void draw(long id, int x, int y, int r, int g, int b) {
        if (out == null) return;
        out.printf("rectu,%d,%d,%d,%d,%d,%d\n", id, x, y, r, g, b);
    }

    private void drawWall(long id, int x, int y, int r, int g, int b) {
        if (out == null) return;
        out.printf("rectw,%d,%d,%d,%d,%d,%d\n", id, x, y, r, g, b);
    }

}
