import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PathFinder {
    HashMap<Point, Path> viewed = new HashMap();
    private List<Path> paths = new ArrayList();
    private OverMind overMind;

    public PathFinder(OverMind overMind) {
        this.overMind = overMind;
    }

    public Path getWay(Point start, final Point target, boolean safe, int limit) {
        if (target.x == start.x && target.y == start.y) return new Path(start);
        Path path = findWay(new EndPointChecker() {
            @Override
            public boolean check(Point p) {
                return target.x == p.x && target.y == p.y;
            }
        },
                start, safe, limit);
        return path;
    }

    public Path getAttackPoint(final Double range, Point start, final Point enemyTr, int limit) {
        if (overMind.world.isVisible(range, start.x, start.y, start.z, enemyTr.x, enemyTr.y, enemyTr.z))
            return new Path(start);
        Path path = findWay(new EndPointChecker() {
            @Override
            public boolean check(Point p) {
                return overMind.world.isVisible(range, p.x, p.y, p.z, enemyTr.x, enemyTr.y, enemyTr.z);
            }
        },
                start, false, limit);
        return path;
    }

    public Path getWayBefore(Point start, final Point target, boolean safe, int limit) {
        if (target.getMapAround2D().contains(start)) return new Path(start); //уже пришли
        final List<Point> endPoints = target.getFreeAround2D();
        Path path = findWay(new EndPointChecker() {
            @Override
            public boolean check(Point p) {
                return endPoints.contains(p);
            }
        },
                start, safe, limit);
        return path;
    }

    public Path getWayToSafePoint(Point tr, int limit) {
        if (overMind.isSafe(tr)) return new Path(tr);
        Path path = findWay(new EndPointChecker() {
            @Override
            public boolean check(Point p) {
                return overMind.isSafe(p);
            }
        },
                tr, false, limit);
        return path;
    }


    private Path findWay(EndPointChecker checker, Point start, boolean safe, int limit) {
        Path result = null;
        viewed.clear();
        paths.clear();
        Path startPath = new Path(start);
        viewed.put(start, startPath);
        paths.add(startPath);
        if (checker.check(start)) return startPath;
        while (!paths.isEmpty()) {
            Path path = paths.get(0);
            paths.remove(0);
            if (path.cost() <= limit && (result == null || result.cost() > path.cost())) {
                for (Point p : path.getEnd().getFreeAround()) {
                    if (safe && !overMind.isSafe(p)) continue;
                    Path newPath = new Path(path);
                    if (step(newPath, p)) {
                        if (checker.check(p)) {
                            paths.remove(newPath);
                            if (newPath.cost() <= limit &&(result == null || result.cost() > newPath.cost()))
                                result = newPath;
                        }
                    }
                }
            }
        }
        return result;
    }

    boolean step(Path newPath, Point p) {
        newPath.add(p);
        Path viewedPath = viewed.get(p);
        if (viewedPath == null) {
            paths.add(newPath);
            viewed.put(p, newPath);
            return true;
        } else if (viewedPath.cost() > newPath.cost()) {
            paths.remove(viewedPath);
            paths.add(newPath);
            viewed.put(p, newPath);
            return true;
        }
        return false;
    }

    public Path getDistancePoint(final double range, final Point start, final Point enemyTr, int limit) {
        if (start.getDistanceTo(enemyTr) <= range)
            return new Path(start);
        Path path = findWay(new EndPointChecker() {
            @Override
            public boolean check(Point p) {
                return start.getDistanceTo(enemyTr) <= range;
            }
        },
                start, false, limit);
        return path;
    }

    interface EndPointChecker {
        public boolean check(Point p);
    }


}
