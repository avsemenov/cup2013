import model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author: semenov
 */
public class TrooperStrategy {
    OverMind overMind;
    Move move;
    World world;
    Game game;
    Trooper trooper;
    int tic;
    int moveIndex;
    boolean underAttack;
    private Program program = null;
    Enemy targetEnemy = null;
    private Point targetPoint = null;

    public TrooperStrategy(OverMind overMind) {
        this.overMind = overMind;
        this.trooper = overMind.trooper;
    }

    public boolean isLive() {
        return tic == overMind.tic;
    }

    private boolean addEnd() {
        return program.add(new ActionEnd());
    }

    public boolean addAttack(Enemy en) {
        if (program == null) program = new Program(getPoint());
        return program.add(new ActionAttack(en, program.getEnd(), trooper.getShootCost()));
    }

    public boolean addPath(Path path) {
        if (program == null) {
            program = new Program(path);
            return true;
        }
        path.remove(0);
        return program.addAll(path);
    }

    public boolean grenade(Point p) {
        if (!trooper.isHoldingGrenade() || trooper.getActionPoints() < game.getGrenadeThrowCost()) return false;
        move.setAction(ActionType.THROW_GRENADE);
        move.setX(p.x);
        move.setY(p.y);
        return true;
    }

    public boolean eatFieldRation() {
        if (!trooper.isHoldingFieldRation() || trooper.getActionPoints() < game.getFieldRationEatCost()) return false;
        move.setAction(ActionType.EAT_FIELD_RATION);
        return true;
    }

    class Program extends ArrayList<Action> {
        static final int STEP_OK = 0;
        static final int STEP_WAITE = 1;
        static final int END = 2;
        private static final int EXPIRED = 3;
        int worldHash = 0;
        private int step = 1;

        public Action getEnd() {
            return get(size() - 1);
        }

        public Program(Path path) {
            super(path);
            worldHash = overMind.worldHash();
        }

        public Program(Point p) {
            super(new Path(p));
            worldHash = overMind.worldHash();
        }

        public int makeStep() {
            if (worldHash != overMind.worldHash()) {
                overMind.debugLog("-expired ");
                return EXPIRED;
            }
            if (step < size()) {
                if (get(step).run(TrooperStrategy.this)) {
                    step++;
                    overMind.debugLog("-ok ");
                    return STEP_OK;
                }
                overMind.debugLog("-waite ");
                return STEP_WAITE;
            }
            overMind.debugLog("-end ");
            return END;
        }
    }

    public void move(Trooper self, World world, Game game, Move move) {
        this.game = game;
        this.move = move;
        this.trooper = self;
        this.world = world;
        if (overMind.newTurn) program = null;
        if (programStep()) return;
        if (overMind.isEnemy()) {
            overMind.debugLog(" attack ");
            attackMode();
        } else {
            overMind.debugLog(" free ");
            freeMode();
        }
        programStep();
    }

    private boolean programStep() {
        if (program == null) return false;
        overMind.debugLog(" program");
        if (program.makeStep() < Program.END) {
            return true;
        }
        program = null;
        return false;
    }

    protected void attackMode() {
        if (findAndGrenade()) return;
        if (findAndKill()) return;
        if (needHeal()) return;
    }

    protected void freeMode() {
        if (needHeal()) return;
//        if (coverEnemy()) return;
        if (moveToFriend()) return;
        if (findBonus()) return;
        if (randomMove()) return;
    }

    private boolean coverEnemy() {
        Point p = null;
        if (underAttack) {
            List<Point> points = getPoint().getFreeAround2D();
            if (points.size() < 1) return false;
            Random random = new Random();
            stepTo(points.get(random.nextInt(points.size() - 1)));
        } else {
            TrooperStrategy help = overMind.getUnderAttaked();
            if (help == null) return false;
            p = overMind.ff.getFreePoint(help.getPoint());
        }
        if (p == null) return false;
        Path path = overMind.pf.getWay(getPoint(), p, false, 200);
        if (path == null) return false;
        return moveToPath(path.trunkToCost(trooper.getActionPoints()));
    }

    boolean randomMove() {
        overMind.debugLog(" randomMove ");
//        overMind.getNearestFriend()
//        return moveToPath(overMind.getCommand(trooper.getActionPoints()));
        return moveToPath(overMind.getMainPoint());
    }

    boolean needHeal() {
        overMind.debugLog(" needHeal ");
        if (trooper.getMaximalHitpoints() > trooper.getHitpoints()) {
            if (trooper.isHoldingMedikit() && trooper.getActionPoints() >= game.getMedikitUseCost()
                    && (trooper.getActionPoints() - trooper.getMaximalHitpoints() >= game.getMedikitHealSelfBonusHitpoints() || !overMind.isMedicExist())) {
                move.setAction(ActionType.USE_MEDIKIT);
                move.setDirection(Direction.CURRENT_POINT);
                overMind.debugLog(" сам лечусь ");
                return true;
            }
            if (overMind.isMedicExist()) {
                safeMoveBeforeMedic(overMind.medic.getPoint());
                overMind.debugLog(" к медику ");
                return true;
            }
        }
        return false;
    }

    boolean findAndKill() {
        overMind.debugLog(" findAndKill ");
        if (overMind.isEnemy()) {
            Path path = getNearestAttackPoint();
            if (path == null) return false;
            path = path.trunkToCost(trooper.getActionPoints());
            if (targetEnemy.heatPoints <= ((trooper.getActionPoints() - path.cost()) / trooper.getShootCost()) * trooper.getDamage()
                    || overMind.getPlayer(targetEnemy.trooper).isStrategyCrashed()) {
                overMind.debugLog(" добить ");
                addPath(path);
                addAttack(targetEnemy, trooper.getActionPoints() - path.cost());
                return true;
            }
            Path safe = overMind.pf.getWayToSafePoint(path.getEnd(), trooper.getActionPoints() - path.cost());
            if (safe != null && trooper.getActionPoints() >= path.cost() + trooper.getShootCost()
                    && trooper.getActionPoints() >= path.cost() + safe.cost()
                    && !overMind.isKill(path.getEnd(), trooper.getHitpoints())) {
                overMind.debugLog(" может не убьют, если что убегу ");
                addPath(path);
                addAttack(targetEnemy, trooper.getActionPoints() - path.cost());
                return true;
            }
            if (trooper.getActionPoints() >= path.cost() + trooper.getShootCost()
                    && trooper.getActionPoints() >= path.cost()
                    && !overMind.isKill(path.getEnd(), trooper.getHitpoints())) {
                overMind.debugLog(" может не убьют ");
                addPath(path);
                addAttack(targetEnemy, trooper.getActionPoints() - path.cost());
                return true;
            }
            if (safe != null && trooper.getActionPoints() >= path.cost() + safe.cost() + trooper.getShootCost()) {
                overMind.debugLog(" бить и убежать ");
                addPath(path);
                addAttack(targetEnemy, trooper.getActionPoints() - path.cost() - safe.cost());
                addPath(safe);
                return true;
            }
            Path hiddenPath = overMind.trunkToSafe(path);
            if (hiddenPath != null && hiddenPath.size() > 2) {
                overMind.debugLog(" подкрадываемся ");
                addPath(hiddenPath);
//                addEnd();
                return true;
            }
            Path retreat = overMind.pf.getWayToSafePoint(getPoint(), trooper.getActionPoints() + getHavingBoost());
            if (retreat != null) {
                if (retreat.size() < 2) return false; //убежали
                overMind.debugLog(" убежать ");
                if (retreat.cost() > trooper.getActionPoints()) {
                    retreat.add(retreat.size() - 1, new ActionEat(retreat.get(retreat.size() - 2)));
                }
                addPath(retreat);
//                addEnd();  //что дальше?
                return true;
            }
            if (trooper.getActionPoints() >= trooper.getShootCost() + path.cost()) {
                overMind.debugLog(" не убежать, деремся ");
                addPath(path);
                addAttack(targetEnemy, trooper.getActionPoints() - path.cost());
                return true;
            }
        }
        return false;
    }

    boolean isHavingBoost() {
        return trooper.getActionPoints() >= game.getFieldRationEatCost() && trooper.isHoldingFieldRation();
    }

    int getHavingBoost() {
        return isHavingBoost() ? game.getFieldRationBonusActionPoints() - game.getFieldRationEatCost() : 0;
    }

    boolean findAndGrenade() {
        overMind.debugLog(" findAndGrenade ");
        if (!trooper.isHoldingGrenade()) return false;
        if (overMind.isEnemy()) {
            Path path = getNearestGrenadePoint();
            int damage = game.getGrenadeDirectDamage();
            if (path == null) {
                path = getNearestExtendGrenadePoint();
                damage = game.getGrenadeCollateralDamage();
            }
            if (path == null) return false;
            if (targetEnemy.heatPoints <= damage) {
                overMind.debugLog(" добить ");
                addPath(path);
                addGrenade(targetPoint);
                return true;
            }
            if (trooper.getActionPoints() >= path.cost() + trooper.getShootCost()
                    //    && trooper.getActionPoints() >= path.cost() + safe.cost()
                    && !overMind.isKill(path.getEnd(), trooper.getHitpoints())) {
                overMind.debugLog(" может не убьют ");
                addPath(path);
                addGrenade(targetPoint);
                return true;
            }
//            Path safe = overMind.pf.getWayToSafePoint(path.getEnd(), trooper.getActionPoints()-path.cost());
//            if (safe!=null && trooper.getActionPoints() >= path.cost() + safe.cost() + trooper.getShootCost()) {
//                overMind.debugLog(" бить и убежать ");
//                addPath(path);
//                addAttack(targetEnemy, trooper.getActionPoints() - path.cost() - safe.cost());
//                addPath(safe);
//                return true;
//            }
//            if (trooper.getActionPoints() >= game.getGrenadeThrowCost()) {
//                overMind.debugLog(" не убежать, деремся ");
//                addPath(path);
//                addGrenade(targetEnemy);
//                return true;
//            }
        }
        return false;
    }

    public Path getNearestGrenadePoint() {
        int points = trooper.getActionPoints() - game.getGrenadeThrowCost();
        if (trooper.isHoldingFieldRation())
            points += game.getFieldRationBonusActionPoints() - game.getFieldRationEatCost();
        if (points < 1) return null;
        targetEnemy = null;
        targetPoint = null;
        Path enemyPath = null;
        for (Enemy en : overMind.getEnemies()) {
            Path path = overMind.pf.getDistancePoint(game.getGrenadeThrowRange(), getPoint(), en.getPoint(), points);
            if (path != null) {
                if (enemyPath == null || path.cost() < enemyPath.cost()) {
                    enemyPath = path;
                    targetEnemy = en;
                    targetPoint = targetEnemy.getPoint();
                }
            }
        }
        if (enemyPath != null && enemyPath.cost() > trooper.getActionPoints() - game.getGrenadeThrowCost()) {
            enemyPath.add(new ActionEat(enemyPath.getEnd()));
        }
        return enemyPath;
    }

    public Path getNearestExtendGrenadePoint() {
        int points = trooper.getActionPoints() - game.getGrenadeThrowCost();
        if (trooper.isHoldingFieldRation())
            points += game.getFieldRationBonusActionPoints() - game.getFieldRationEatCost();
        if (points < 1) return null;
        targetPoint = null;
        targetEnemy = null;
        Path enemyPath = null;
        for (Enemy en1 : overMind.getEnemies()) {
            for (Point en : en1.getPoint().getMapAround2D()) {
                Path path = overMind.pf.getDistancePoint(game.getGrenadeThrowRange(), getPoint(), en, points);
                if (path != null) {
                    if (enemyPath == null || path.cost() < enemyPath.cost()) {
                        enemyPath = path;
                        targetPoint = en;
                        targetEnemy = en1;
                    }
                }
            }
        }
        if (enemyPath != null && enemyPath.cost() > trooper.getActionPoints() - game.getGrenadeThrowCost()) {
            enemyPath.add(new ActionEat(enemyPath.getEnd()));
        }
        return enemyPath;
    }

    private boolean addAttack(Enemy enemy, int actionPoints) {
        for (int i = (actionPoints / trooper.getShootCost()); i > 0; i--)
            if (!addAttack(enemy)) return false;
        return true;
    }

    private boolean addGrenade(Enemy en) {
        if (program == null) program = new Program(getPoint());
        return program.add(new ActionGrenade(en.getPoint(), program.getEnd()));
    }

    private boolean addGrenade(Point en) {
        if (program == null) program = new Program(getPoint());
        return program.add(new ActionGrenade(en, program.getEnd()));
    }

    boolean isAlone() {
        return overMind.squadCount <= 1;
    }

    public Path getNearestAttackPoint() {
        targetEnemy = null;
        Path enemyPath = null;
        for (Enemy en : overMind.getEnemies()) {
            Path path = overMind.pf.getAttackPoint(trooper.getShootingRange(), getPoint(), en.getPoint(), 200);
            if (path != null) {
                if (enemyPath == null || path.cost() < enemyPath.cost()) {
                    enemyPath = path;
                    targetEnemy = en;
                }
            }
        }
        return enemyPath;
    }

    boolean setGoodPosition(Trooper en) {
        if (trooper.getStance() == TrooperStance.PRONE) return false;
        if ((trooper.getActionPoints() % trooper.getShootCost()) >= game.getStanceChangeCost()
                && world.isVisible(trooper.getShootingRange(), trooper.getX(), trooper.getY(), nextLoverStance(), en.getX(), en.getY(), en.getStance())) {
            move.setAction(ActionType.LOWER_STANCE);
            return true;
        }
        int shootCount = trooper.getActionPoints() / trooper.getShootCost();
        if (shootCount * trooper.getDamage() >= en.getHitpoints()) return false; //итак умрет
        if (trooper.getActionPoints() >= trooper.getShootCost() + game.getStanceChangeCost() && world.isVisible(trooper.getShootingRange(), trooper.getX(), trooper.getY(), nextLoverStance(), en.getX(), en.getY(), en.getStance())) {
            move.setAction(ActionType.LOWER_STANCE);
            return true;
        }
        return false;
    }

    private TrooperStance nextLoverStance() {
        if (trooper.getStance() == TrooperStance.STANDING) return TrooperStance.KNEELING;
        if (trooper.getStance() == TrooperStance.KNEELING) return TrooperStance.PRONE;
        return null;
    }

    private TrooperStance nextUpperStance() {
        if (trooper.getStance() == TrooperStance.PRONE) return TrooperStance.KNEELING;
        if (trooper.getStance() == TrooperStance.KNEELING) return TrooperStance.STANDING;
        return null;
    }

    boolean attack(Enemy en) {
        if (trooper.getShootCost() > trooper.getActionPoints()) {
            return false;
        }
        move.setAction(ActionType.SHOOT);
        move.setX(en.trooper.getX());
        move.setY(en.trooper.getY());
        overMind.setLastAttacked(en);
        if (en.takeDamage(trooper.getDamage())) {
            overMind.killed(en);
        }
        return true;
    }

    boolean stepTo(Point step) {
        if (step == null) {
            return false;
        }
        if (trooper.getActionPoints() < step.distance(getPoint())) {
            overMind.debugLog(" no Actions.");
            return false;
        }
        if (step.z == trooper.getStance()) {
            move.setAction(ActionType.MOVE);
            move.setX(step.x);
            move.setY(step.y);
        } else if (step.z == nextLoverStance()) {
            move.setAction(ActionType.LOWER_STANCE);
        } else {
            move.setAction(ActionType.RAISE_STANCE);
        }
        return true;
    }

    boolean safeMoveBeforeMedic(Point point) {
//        overMind.trooper = overMind.medic.trooper;
        Path path = overMind.pf.getWayToSafePoint(point, 200);
        if (path == null) return false;
        Point healPoint = path.getEnd();
        Path healWay = overMind.pf.getWay(getPoint(), healPoint, false, 200);
        if (healWay == null) return false;
        if (healWay.cost() <= trooper.getActionPoints()) {
            return moveToPath(healWay);
        }
        //TODO переделать на поиск безопасной конечной точки
        healWay = overMind.pf.getWay(getPoint(), healPoint, true, trooper.getActionPoints());
        if (healWay == null) return false;
        return moveToPath(healWay);
    }

    boolean moveToPath(Path path) {
        if (path != null && path.size() > 1) {
            program = new Program(path);
            return true;
        }
        program = null;
        return false;
    }

    boolean moveToFriend() {
        overMind.debugLog(" moveToFriend ");
        if (isAlone()) return false;
        Path path = overMind.getFarestFriend(200);
        if (path != null && path.size() > 3)
            return moveToPath(path);
        return false;
    }

    public void setTrooper(Trooper trooper) {
        this.underAttack = this.underAttack || (this.trooper.getHitpoints() > trooper.getHitpoints());
        this.trooper = trooper;
        this.tic = overMind.tic;
        this.moveIndex = overMind.moveIndex;
    }

    public Trooper getTrooper() {
        return trooper;
    }

    public Point getPoint() {
        return new Point(trooper);
    }

    public int getMoveCost() {
        return overMind.getMoveCost(trooper.getStance());
    }

    boolean findBonus() {
        overMind.debugLog(" findBonus ");
        if (trooper.isHoldingFieldRation() && trooper.isHoldingGrenade() && trooper.isHoldingMedikit()) return false;
        Path path = getNearestBonus();
        if (path == null) return false;
        overMind.debugLog(" за бонусом ");
        path = path.trunkToCost(trooper.getActionPoints());
        overMind.setMainPoint(path.getEnd());
//        if (path.size() > 4) {
//            path.cutEnd();
//            path.add(path.get(path.size() - 2));
//        }
        return moveToPath(path);
    }

    Path getNearestBonus() {
        Point curPoint = new Point(trooper);
        Path nearestPath = null;
        for (Bonus bonus : world.getBonuses()) {
            if (isHolding(bonus.getType())) continue;
            Path path = overMind.pf.getWay(curPoint, new Point(bonus), false, 200);
            if (path != null) {
                if (nearestPath == null || nearestPath.cost() > path.cost()) {
                    nearestPath = path;
                }
            }
        }
        return nearestPath;
    }

    boolean isHolding(BonusType bonusType) {
        return trooper.isHoldingGrenade() && bonusType == BonusType.GRENADE
                || trooper.isHoldingFieldRation() && bonusType == BonusType.FIELD_RATION
                || trooper.isHoldingMedikit() && bonusType == BonusType.MEDIKIT;
    }
}
