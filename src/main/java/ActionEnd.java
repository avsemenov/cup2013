/**
 * @author: semenov
 */
public class ActionEnd extends Action {
    public ActionEnd() {
        super(new Point(0,0,null));
    }

    @Override
    public boolean run(TrooperStrategy ts) {
        return true;
    }

    @Override
    public int cost(Action prev) {
        return 0;
    }
}
