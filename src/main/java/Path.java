import java.util.ArrayList;

public class Path extends ArrayList<Action> {
    private int cost = 0;

    public Path() {
        super();
    }

    public Path(Path path) {
        super(path);
    }

    public Path(Point point) {
        super();
        add(new Action(point));
    }

    public boolean add(Point point) {
        return add(new Action(point));
    }

    public Point cutEnd() {
        return remove(size() - 1);
    }

    public Point getEnd() {
        return get(size() - 1);
    }

    public int cost() {
        if (size() < 2) return 0;
        if (cost != 0) return cost;
        for (int i = 1; i < size(); i++) {
            cost += get(i).cost(get(i - 1));
        }
        return cost;
    }

    public Path trunkToCost(int cost) {
        if (size() < 2) return new Path(this);
        Path path = new Path(get(0));
        int c = cost;
        for(int i=1;i<size();i++) {
            c -= get(i).cost(get(i - 1));
            if (c < 0) return path;
            path.add(get(i));
        }
        return path;
    }

//    public int cost() {
//        if (size() < 2) return 0;
//        if (cost != 0) return cost;
//        for (int i = 1; i < size(); i++) {
//            cost += get(i).distance(get(i - 1));
//        }
//        return cost;
//    }
}