import model.Unit;

/**
 * @author semenov
 */
public class Action extends Point {
    public Action(Point p) {
        super(p.x, p.y, p.z);
    }

    public boolean run(TrooperStrategy ts) {
        return ts.stepTo(this);
    }

    public int cost(Action prev) {
        return this.distance(prev);
    }
}
