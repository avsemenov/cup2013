import model.*;

import java.util.*;

/**
 * @author: semenov
 */
public class OverMind {
    private static final boolean DEBUG = true;
    Trooper trooper = new Trooper(-100, 0, 0, 0, 0, true, TrooperType.COMMANDER, TrooperStance.STANDING, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, false, false);
    World world;
    Game game;
    Random random = new Random();
    int moveIndex = -1;
    Map<Long, Enemy> enemy = new HashMap();
    Point pint = new Point(0, 0, TrooperStance.STANDING);
    final TrooperStrategy medic = new Medic(this);
    final TrooperStrategy soldier = new Soldier(this);
    final TrooperStrategy commander = new Commander(this);
    final Map<TrooperType, TrooperStrategy> squad = new HashMap(3);

    {
        squad.put(TrooperType.FIELD_MEDIC, medic);
        squad.put(TrooperType.SOLDIER, soldier);
        squad.put(TrooperType.COMMANDER, commander);
    }

    MainPath mainPath = new MainPath();
    Map<TrooperStance, Integer> moveCosts = new HashMap(3);
    List<Trooper> wounded = new ArrayList();

    PathFinder pf = new PathFinder(this);
    FreeFinder ff = new FreeFinder(this);
    private boolean initStep = true;
    int tic = 0;
    boolean newTurn;
    private CellType[][] cells;
    int turn = 0;
    private Enemy lastAttacked = null;
    int squadCount;
    private int lastScore;
    private Point mainPoint;

    public void init(Trooper self, World world, Game game, Move move) {
        newTurn = (this.trooper.getId() != self.getId() || world.getMoveIndex() != moveIndex);
        moveIndex = world.getMoveIndex();
        if (newTurn) turn++;
        this.trooper = self;
        this.world = world;
        this.game = game;
        tic++;

        if (lastAttacked != null) {
            if (lastScore == getPlayer().getScore()) {
                enemy.remove(lastAttacked.trooper.getId());
            } else {
                if (!newTurn) lastAttacked.expireTurn += 1;
            }
        }
        lastAttacked = null;

        wounded.clear();
        squadCount = 0;
        boolean enemyExist = false;
        for (Trooper tr : world.getTroopers()) {
            if (tr.isTeammate()) {
                if (tr.getMaximalHitpoints() > tr.getHitpoints()) {
                    wounded.add(tr);
                }
                squad.get(tr.getType()).setTrooper(tr);
                squadCount++;
            } else {
                enemyExist = true;
            }
        }
        if (enemyExist) {
            for (Trooper tr : world.getTroopers()) {
                if (!tr.isTeammate()) {
                    enemy.put(tr.getId(), new Enemy(this, tr));
                }
            }
        }

        if (initStep) initConstants();

//        if (newTurn) {
        Iterator<Enemy> it = enemy.values().iterator();
        while (it.hasNext()) {
            Enemy e = it.next();
            if (e.isExpired()) {
                it.remove();
//                    enemy.remove(e.trooper.getId());
            } else if (e.isInvisible()) {
                for (TrooperStrategy ts : squad.values()) {
                    if (ts.isLive() && world.isVisible(ts.trooper.getVisionRange(), ts.trooper.getX(), ts.trooper.getY(), ts.trooper.getStance(), e.trooper.getX(), e.trooper.getY(), e.trooper.getStance())) {
                        it.remove();
                        break;
                    }
                }
            }
//            }
        }

        TrooperStrategy ts = squad.get(self.getType());
        ts.move(self, world, game, move);
    }

    private Player getPlayer() {
        for (Player pl : world.getPlayers()) {
            if (pl.getId() == trooper.getPlayerId()) return pl;
        }
        return null;
    }

    public void setLastAttacked(Enemy lastAttacked) {
        this.lastAttacked = lastAttacked;
        lastScore = getPlayer().getScore();
    }

    public Enemy getLastAttacked() {
        return lastAttacked;
    }

    Player getPlayer(Trooper en) {
        for (Player pl : world.getPlayers()) {
            if (pl.getId() == en.getPlayerId()) {
                return pl;
            }
        }
        return null;
    }

    private void initConstants() {
        moveCosts.put(TrooperStance.PRONE, game.getProneMoveCost());
        moveCosts.put(TrooperStance.KNEELING, game.getKneelingMoveCost());
        moveCosts.put(TrooperStance.STANDING, game.getStandingMoveCost());
        Point.setOverMind(this);
        cells = world.getCells();
//        pint = ff.getFreePoint(solder.getX(), solder.getY());
        initStep = false;
    }

    public boolean isMedicExist() {
        return squad.get(TrooperType.FIELD_MEDIC).isLive();
    }

    int getMoveCost(TrooperStance stance) {
        return moveCosts.get(stance);
    }

    public boolean isEnemy() {
        return !enemy.isEmpty();
    }

    public Collection<Enemy> getEnemies() {
        return enemy.values();
//        ArrayList<Trooper> enemyList = new ArrayList();
//        for (Enemy en : enemy.values()) {
//            enemyList.add(en.trooper);
//        }
//        return enemyList;
    }

    public boolean isWounded() {
        return !wounded.isEmpty();
    }

    public TrooperStrategy getUnderAttaked() {
        for (TrooperStrategy ts : squad.values()) {
            if (ts.underAttack) return ts;
        }
        return null;
    }

    public Path getFarestFriend(int limit) {
        Point curPoint = new Point(trooper);
        Path friendPath = null;
        for (TrooperStrategy ts : squad.values()) {
            if (!ts.isLive() || ts.trooper.getId() == trooper.getId()) continue;
            Path path = pf.getWayBefore(curPoint, ts.getPoint(), false, limit);
            if (path != null) {
                if (friendPath == null || friendPath.cost() < path.cost()) {
                    friendPath = path;
                }
            }
        }
        return friendPath;
    }

    public void setMainPoint(Point mainPoint) {
        this.mainPoint = mainPoint;
    }

    public Path getMainPoint() {
        if (mainPoint == null || !isFreePoint(mainPoint) || mainPoint.x == trooper.getX() && mainPoint.y == trooper.getY()) {
            mainPoint = randomFreePoint();
        }
        return pf.getWay(new Point(trooper), mainPoint, false, 200);
    }

    private Point randomFreePoint() {
        Point p = new Point(random.nextInt(world.getWidth() - 1), random.nextInt(world.getHeight() - 1), TrooperStance.STANDING);
        if (!isFreePoint(p)) p = ff.getFreePoint(p.x, p.y);
        return p;
    }

    class MainPath {
        Path mainPath;
        int pos = 0;
        long leader = -1;
        Set<Long> hvost = new HashSet<Long>(2);
        private Point point;
        Random random = new Random();
        List<Point> conas = new ArrayList<Point>();

        {
            conas.add(new Point(0, 0, null));
            conas.add(new Point(29, 0, null));
            conas.add(new Point(29, 19, null));
            conas.add(new Point(0, 19, null));
        }

        public void nexRandomTarget() {
            if (mainPath != null && pos + 6 > mainPath.size()) {
                conas.remove(0);
            }
            pos = 0;
            Point p;
            if (conas.size() > 0) {
                p = ff.getFreePoint(conas.get(0));
            } else {
//        Point p = new Point(trooper.getX() + randomX.nextInt(10) - 5, trooper.getY() + randomY.nextInt(10) - 5, TrooperStance.STANDING);
                p = new Point(random.nextInt(world.getWidth() - 1), random.nextInt(world.getHeight() - 1), TrooperStance.STANDING);
            }
            if (!isFreePoint(p)) p = ff.getFreePoint(p.x, p.y);
            mainPath = pf.getWay(new Point(trooper), p, false, 200);
        }

        public Path getCommand(int actions) {
            if (trooper.getStance() != TrooperStance.STANDING) {
                return pathToUp();
            }
            if (leader == -1) {
                leader = trooper.getId();
                nexRandomTarget();
            }
            if (trooper.getId() == leader) {
                hvost.clear();
                Point startPoint = new Point(trooper);
                if (mainPath == null || !startPoint.equals(mainPath.get(pos))) {
                    nexRandomTarget();
                }
                int steps = actions / 2;
                if (mainPath == null || pos + steps > mainPath.size()) {
                    nexRandomTarget();
                    if (mainPath == null || pos + steps > mainPath.size()) {
                        leader = -1;
                        return null;
                    }
                }
                Path path = new Path(startPoint);
                if (steps < 2) return path;
                for (int i = pos + 1; i < pos + steps; i++) {
                    Point p = mainPath.get(i);
                    if (!isFreePoint(p)) {
                        nexRandomTarget();
                        return getCommand(actions);
                    }
                    path.add(p);
                }
                pos += steps - 2;
                path.add(mainPath.get(pos));
                return path;
            }
            if (hvost.contains(trooper.getId())) {
                leader = trooper.getId();
                return getCommand(actions);
            }
            hvost.add(trooper.getId());
            Point target;
            if (pos < hvost.size()) {
                target = ff.getFreePoint(mainPath.get(pos));
            } else {
                target = mainPath.get(pos - hvost.size());
            }
            Path out = pf.getWay(new Point(trooper), target, false, 200);
            if (out != null && out.cost() < actions) out.add(new ActionEnd());
            return out;
        }

    }

    public Path pathToUp() {
        Point point = new Point(trooper);
        Path path = new Path(point);
        while (point.nextUpperStance() != null) {
            point = new Point(point.x, point.y, point.nextUpperStance());
            path.add(point);
        }
        return path;
    }

    public Path getCommand(int actions) {
        return mainPath.getCommand(actions);
    }

    public boolean isFreePoint(Point point) {
        return isFreePoint(point.x, point.y, false);
    }

    public boolean isFreePoint(int x, int y) {
        return isFreePoint(x, y, false);
    }

    public boolean isFreePoint(int x, int y, boolean noTroopers) {
//        debugLog(" isFree %d, %d ", x, y);
        if (x < 0 || y < 0
                || x >= world.getWidth() || y >= world.getHeight()
                || cells[x][y] != CellType.FREE)
            return false;
        if (!noTroopers) {
            for (Trooper tr : world.getTroopers()) {
                if (tr.getId() == trooper.getId()) continue;
                if (tr.getX() == x && tr.getY() == y) return false;
            }
        }
        return true;
    }

    public int getChangeStance() {
        return game.getStanceChangeCost();
    }

    boolean isCommanderBonus(Point p) {
        return true;
//        if (isWounded()) return true;
//        if (trooper.getType() == TrooperType.COMMANDER) return true;
//        if (!commander.isLive()) return true;
//        if (commander.getTrooper().getDistanceTo(trooper.getX(), trooper.getY()) > game.getCommanderAuraRange()) return true;
//        return commander.getTrooper().getDistanceTo(p.x, p.y) < game.getCommanderAuraRange();
    }


    void debugLog(String format, Object... args) {
        if (!DEBUG) return;
        System.out.printf(format, args);
    }

    void debugLog(Object msg) {
        if (!DEBUG) return;
        System.out.print(msg);
    }


    public Path getNearestFriend(int limit) {
        Point curPoint = new Point(trooper);
        Path friendPath = null;
        for (TrooperStrategy ts : squad.values()) {
            if (!ts.isLive() || ts.trooper.getId() == trooper.getId()) continue;
            Path path = pf.getWayBefore(curPoint, ts.getPoint(), false, limit);
            if (path != null) {
                if (friendPath == null || friendPath.cost() > path.cost()) {
                    friendPath = path;
                }
            }
        }
        return friendPath;
    }

    public void killed(Enemy en) {
        enemy.remove(en.trooper.getId());
    }

    public boolean isSafe(Point p) {
        return isSafe(p.x, p.y, p.z);
    }

    public boolean isSafe(int x, int y, TrooperStance z) {
        for (Enemy en : getEnemies()) {
            if (world.isVisible(en.trooper.getVisionRange(), en.trooper.getX(), en.trooper.getY(), en.trooper.getStance(), x, y, z))
                return false;
        }
        return true;
    }

    public int worldHash() {
        int[] hash = new int[enemy.size() * 4];
        int i = 0;
        for (Enemy en : getEnemies()) {
            hash[i++] = (int) en.trooper.getId();
            hash[i++] = en.trooper.getX();
            hash[i++] = en.trooper.getY();
            hash[i++] = en.trooper.getStance().ordinal();
        }
        return Arrays.hashCode(hash);
    }

    public Path trunkToSafe(Path path) {
        if (path == null || !isSafe(path.get(0))) return null;
        Path trunked = new Path();
        for (Point p : path) {
            if (!isSafe(p)) return trunked;
            trunked.add(p);
        }
        return trunked;
    }

    public int attackersCount(Enemy targetEnemy) {
        int cnt = 0;
        for (TrooperStrategy ts : squad.values()) {
            if (ts.isLive() && targetEnemy.equals(ts.targetEnemy)) cnt++;
        }
        return cnt;
    }

    public boolean isKill(Point point, int hitpoints) {
        int damage = 0;
        for (Enemy en : getEnemies()) {
            damage += en.canMakeDamage(point);
        }
        return damage >= hitpoints;
    }
}
