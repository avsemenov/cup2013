public class ActionAttack extends Action {
    private final Enemy enemy;
    private final int cost;

    public ActionAttack(Enemy targetEnemy, Point from, int cost) {
        super(from);
        enemy = targetEnemy;
        this.cost = cost;
    }

    @Override
    public boolean run(TrooperStrategy ts) {
        return ts.attack(enemy);
    }

    @Override
    public int cost(Action prev) {
        return cost;
    }
}
