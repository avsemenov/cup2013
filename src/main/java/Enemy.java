import model.Trooper;
import model.TrooperType;

import java.util.List;

/**
 * @author: semenov
 */
public class Enemy {
    int expireTurn;
    final OverMind overMind;
    private final int createTurn;
    public Trooper trooper;
    public List<Long> see;
    int heatPoints;

    @Override
    public int hashCode() {
        return new Long(trooper.getId()).intValue();
    }

    @Override
    public String toString() {
        return trooper.getType() + "(" + trooper.getX() + "," + trooper.getY() + ")";
    }

    public Enemy(OverMind overMind, Trooper trooper) {
        this.createTurn = overMind.turn;
        this.expireTurn = overMind.turn + overMind.squadCount + 1;
        this.overMind = overMind;
        this.trooper = trooper;
        heatPoints = trooper.getHitpoints();
    }

    public boolean isExpired() {
        return overMind.turn >= expireTurn;
    }

    public void findSee() {
        for (Trooper tr : overMind.world.getTroopers()) {
            if (!tr.isTeammate()) continue;
            if (overMind.world.isVisible(trooper.getVisionRange(), trooper.getX(), trooper.getY(), trooper.getStance(), tr.getX(), tr.getY(), tr.getStance())) {
                see.add(tr.getId());
            }
        }
    }

    public boolean isSeeMe(Trooper tr) {
        return overMind.world.isVisible(trooper.getVisionRange(), trooper.getX(), trooper.getY(), trooper.getStance(), tr.getX(), tr.getY(), tr.getStance());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Enemy) {
            return ((Enemy) obj).trooper.getId() == this.trooper.getId();
        }
        return super.equals(obj);
    }

    public Point getPoint() {
        return new Point(trooper);
    }

    public boolean takeDamage(int damage) {
        heatPoints -= damage;
        return heatPoints <= 0;
    }

    public boolean isInvisible() {
        return createTurn != overMind.turn;
    }

    public boolean isKill(Point point, int hitPoints) {
        return (canMakeDamage(point) >= hitPoints);
    }

    public int canMakeDamage(Point point) {
        int points = trooper.getInitialActionPoints();
        if (trooper.getType() != TrooperType.COMMANDER) points += overMind.game.getCommanderAuraBonusActionPoints();
        Path path = overMind.pf.getAttackPoint(trooper.getShootingRange(), getPoint(), point, points - trooper.getShootCost());
        if (path == null) return 0;
        int damage = points / trooper.getShootCost() * trooper.getDamage(path.getEnd().z);
        return damage;
    }
}
