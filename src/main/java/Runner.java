import model.*;

import java.io.IOException;

public final class Runner {
    private final RemoteProcessClient remoteProcessClient;
    private final String token;
    private int moveIndex = -1;

    public static void main(String[] args) throws IOException {
        if (args.length == 3) {
            new Runner(args).run();
        } else {
            new Runner(new String[]{"localhost", "31001", "0000000000000000"}).run();
        }
    }

    private Runner(String[] args) throws IOException {
        remoteProcessClient = new RemoteProcessClient(args[0], Integer.parseInt(args[1]));
        token = args[2];
    }

    public void run() throws IOException {
        try {
            remoteProcessClient.writeToken(token);
            int teamSize = remoteProcessClient.readTeamSize();
            remoteProcessClient.writeProtocolVersion();
            Game game = remoteProcessClient.readGameContext();

            Strategy[] strategies = new Strategy[teamSize];

            for (int strategyIndex = 0; strategyIndex < teamSize; ++strategyIndex) {
                strategies[strategyIndex] = new MyStrategy();
            }

            PlayerContext playerContext;

            while ((playerContext = remoteProcessClient.readPlayerContext()) != null) {
                Trooper playerTrooper = playerContext.getTrooper();

                Move move = new Move();

                World world = playerContext.getWorld();
                if (world.getMoveIndex() != moveIndex) {
                    System.out.printf("\nTURN %d", world.getMoveIndex());
                    moveIndex = world.getMoveIndex();
                }
                System.out.printf("\n%s(%d,%d)[%d] ", playerTrooper.getType().toString(), playerTrooper.getX(), playerTrooper.getY(), playerTrooper.getActionPoints());
//            debugLog("index= %d/%d/%d ", trooper.getId(), trooper.getPlayerId(), trooper.getTeammateIndex());

                strategies[playerTrooper.getTeammateIndex()].move(playerTrooper, world, game, move);
                remoteProcessClient.writeMove(move);
            }
        } finally {
            remoteProcessClient.close();
        }
    }
}
